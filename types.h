#pragma once

#include <tuple>
#include <cmath>

using real_t = double;
using size_t = long unsigned int;

typedef struct face3d_t
{
  int k1, f1, k2, f2;
  int va, vb, vc;

  bool operator<(face3d_t const &o) const
  {
    return std::tie(this->va, this->vb, this->vc) < std::tie(o.va, o.vb, o.vc);
  }

  bool operator<=(face3d_t const &o) const
  {
    return std::tie(this->va, this->vb, this->vc) <= std::tie(o.va, o.vb, o.vc);
  }

  bool operator>(face3d_t const &o) const
  {
    return std::tie(this->va, this->vb, this->vc) > std::tie(o.va, o.vb, o.vc);
  }

  bool operator>=(face3d_t const &o) const
  {
    return std::tie(this->va, this->vb, this->vc) >= std::tie(o.va, o.vb, o.vc);
  }

} face3d_t;





template<typename T>
class vec3_t {
public:
  T x;
  T y;
  T z;

  vec3_t()
  : x()
  , y()
  , z()
  { }

  vec3_t(T x_, T y_, T z_)
  : x(x_)
  , y(y_)
  , z(z_)
  { }

  vec3_t(vec3_t<T> const&) = default;
  vec3_t(vec3_t<T> &&) = default;

  vec3_t<T>& operator=(vec3_t<T> const&) = default;
  vec3_t<T>& operator=(vec3_t<T> &&) = default;

  void operator=(T const& o) {
    this->x = o;
    this->y = o;
    this->z = o;
  }

  vec3_t<T> operator-() const { // unary -
    auto o(*this);
    o.x = -x;
    o.y = -y;
    o.z = -z;
    return o;
  }

  void operator/=(T const& o) {
    this->x /= o;
    this->y /= o;
    this->z /= o;
  }

  void operator+=(vec3_t<T> const& o) {
    this->x += o.x;
    this->y += o.y;
    this->z += o.z;
  }

  T const length() const {
    return std::sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
  }
};

template<typename T>
inline vec3_t<T> operator*(T const& t, vec3_t<T> const& p) {
  auto o(p);
  o.x *= t;
  o.y *= t;
  o.z *= t;
  return o;
}

template<typename T>
inline vec3_t<T> operator*(vec3_t<T> const& p, T const t) {
  auto o(p);
  o.x *= t;
  o.y *= t;
  o.z *= t;
  return o;
}

template<typename T>
inline vec3_t<T> operator+(vec3_t<T> const& a, vec3_t<T> const& b) {
  auto o(a);
  o.x += b.x;
  o.y += b.y;
  o.z += b.z;
  return o;
}

template<typename T>
inline vec3_t<T> operator-(vec3_t<T> const& a, vec3_t<T> const& b) {
  auto o(a);
  o.x -= b.x;
  o.y -= b.y;
  o.z -= b.z;
  return o;
}

template<typename T>
inline vec3_t<T> hadamard_product(vec3_t<T> const& a, vec3_t<T> const& b) {
  auto o(a);
  o.x *= b.x;
  o.y *= b.y;
  o.z *= b.z;
  return o;
}

template<typename T>
inline T dot_product(vec3_t<T> const& a, vec3_t<T> const& b) {
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

template<typename T>
inline vec3_t<T> cross_product(vec3_t<T> const& a, vec3_t<T> const& b) {
  return vec3_t<T> { a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x };
}

using coord_t = vec3_t<double>;
using real2_t = std::array<real_t, 2>;
using real3_t = vec3_t<real_t>;
