# Solving Maxwell's Equations with Modern C++ and SYCL: A Case Study

This is an example code implemented with SYCL to solve Maxwell's equations using a Discountinuous Galerkin approach on an unstructured mesh.
It can be compiled either using Codeplay's ComputeCpp or using triSYCL.


Most of the code, around 70%, deals with setting up the mesh and related data. Included are a few example meshes of different sizes (.neu files), which you can add as a parameter to the binary.
Certain things still could (should) be optimized, such as the memory layout (not using an array of structs, for example), splitting of the single large kernel into two or three separate ones, and obviously more comments.

Please note: It's one of our research project's example codes, so it favors readability over performance or elegance of implementation.

## How to use with ComputeCpp
The included Makefile.ccpp should compile cleanly with ComputeCpp available in the corresponding include paths and will generate two binaries `mw_cpu` and `mw_gpu`, the first one targeting the first CPU device, the other one targeting the first GPU device. You can increase the numerical order (basically, the accuracy and thus the computational intensity) by adding the parameter `N=x` when calling the Makefile, with `1<=x<=9`.

## How to use with triSYCL
The included Makefile.trisycl should compile cleanly with triSYCL available in the corresponding include paths and will generate a binary called `mw_trisycl`, for execution on the default host device. The warning about using the host device emitted during compilation may be ignored. You can increase the numerical order (basically, the accuracy and thus the computational intensity) by adding the parameter `N=x` when calling the Makefile, with `1<=x<=9`.

