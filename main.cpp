//  Ported to SYCL by Christian Schmitt

// #define ERROR_EVERY_ITERATION
#define RECORD_TIMES

#if defined USE_CPU
#define GROUPSIZE 128
#define RESULTS_FILE "results_cpu.csv"
#elif defined USE_GPU
#define GROUPSIZE 32
#define RESULTS_FILE "results_gpu.csv"
#else
#warning Please define USE_CPU or USE_GPU; using HOST for now
#define GROUPSIZE 4
#define RESULTS_FILE "results_host.csv"
#endif

#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iostream>
#include <CL/sycl.hpp>


#ifdef RECORD_TIMES
#include <chrono>
#ifndef RESULTS_FILE
#error Missing results filename definition (RESULTS_FILE)
#endif
#endif

#include "types.h"

// default to order 3
#ifndef p_N
#define p_N 3
#endif

#if p_N == 1
#include "data3dN01.h"
#elif p_N == 2
#include "data3dN02.h"
#elif p_N == 3
#include "data3dN03.h"
#elif p_N == 4
#include "data3dN04.h"
#elif p_N == 5
#include "data3dN05.h"
#elif p_N == 6
#include "data3dN06.h"
#elif p_N == 7
#include "data3dN07.h"
#elif p_N == 8
#include "data3dN08.h"
#elif p_N == 9
#include "data3dN09.h"
#else
#error Order (p_N) must be between 1 and 9
#endif

#define nodal_points_per_face     ((p_N+1)*(p_N+2)/2)         /* number of dg nodes only on faces */
#define nodal_points_per_element  ((p_N+1)*(p_N+2)*(p_N+3)/6) /* number of dg nodes */

inline size_t const MULT(size_t const x) { return static_cast<size_t>(std::ceil(static_cast<double>(x) / static_cast<double>(GROUPSIZE))) * GROUPSIZE; }

class mw_surface;
class mw_rk;

int main(int argc, char **argv)
{
	if (argc < 2)
	{
	 	std::cerr << "Missing input file" << std::endl;
	 	std::exit(-1);
	}

	// read mesh file
	std::string tmp_line;
	std::ifstream mesh_file;
	mesh_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		mesh_file.open(argv[1]);
	}
	catch (std::ifstream::failure const &e)
	{
		std::cerr << "Error opening mesh file" << std::endl;
		std::exit(-1);
	}

	// ignore first 6 lines
	for (size_t i = 0; i < 6; ++i)
		std::getline(mesh_file, tmp_line);
	size_t total_vertices, mesh_elements;

	// # vertices, # elements
	{
		std::getline(mesh_file, tmp_line);
		std::stringstream tmp_stringstream(tmp_line);
		tmp_stringstream >> total_vertices >> mesh_elements;
	}

	std::vector<coord_t> vertices(total_vertices);
	size_t constexpr mesh_vertices_per_element = 4;
	//size_t constexpr mesh_edges_per_element = 6; // unused
	size_t constexpr mesh_faces_per_element = 4;

	// ignore 2 lines
	for (size_t i = 0; i < 2; ++i)
		std::getline(mesh_file, tmp_line);

	// Reading x,y,z coordinates of vertices
	for (size_t i = 0; i < total_vertices; ++i)
	{
		std::getline(mesh_file, tmp_line);
		std::stringstream tmp_stringstream(tmp_line);
		size_t n;
		tmp_stringstream >> n;
		tmp_stringstream >> vertices[n - 1].x >> vertices[n - 1].y >> vertices[n - 1].z;
	}
	std::cout << "no of vertices = " << total_vertices << ", no of elements = " << mesh_elements << std::endl;

	// ignore 2 lines
	for (size_t i = 0; i < 2; ++i)
		std::getline(mesh_file, tmp_line);

	std::vector<std::vector<size_t>> elements_to_vertices;
	elements_to_vertices.resize(mesh_elements, std::vector<size_t>(mesh_vertices_per_element, 0));
	cl::sycl::buffer<coord_t, 2> b_vertex_coords(cl::sycl::range<2>{mesh_elements, mesh_vertices_per_element});

	// Read number of elements formed by the vertices
	for (size_t i = 0; i < mesh_elements; ++i)
	{
		auto a_vertex_coords = b_vertex_coords.get_access<cl::sycl::access::mode::write>();
		std::getline(mesh_file, tmp_line);
		std::stringstream tmp_stringstream(tmp_line);
		size_t dummy;
		// skip first three columns
		tmp_stringstream >> dummy >> dummy >> dummy;
		tmp_stringstream >> elements_to_vertices[i][0] >> elements_to_vertices[i][1] >> elements_to_vertices[i][2] >> elements_to_vertices[i][3];

		/* correct to 0-index */ //// The .neu file provides 1 index
		--(elements_to_vertices[i][0]);
		--(elements_to_vertices[i][1]);
		--(elements_to_vertices[i][2]);
		--(elements_to_vertices[i][3]);

		for (size_t v = 0; v < mesh_vertices_per_element; ++v)
		{
			//// Get the x,y,z coordinates from vertex number
			a_vertex_coords[{i, v}] = vertices[elements_to_vertices[i][v]];
		}
	}

	vertices.clear();

	// vertex number order
	size_t constexpr vnum[4][3] = {{0, 1, 2}, {0, 1, 3}, {1, 2, 3}, {0, 2, 3}};

	std::vector<face3d_t> myfaces(mesh_elements * mesh_faces_per_element);

	for (size_t k = 0; k < mesh_elements; ++k)
	{
		for (size_t e = 0; e < mesh_faces_per_element; ++e)
		{
			auto a1 = elements_to_vertices[k][vnum[e][0]];
			auto b1 = elements_to_vertices[k][vnum[e][1]];
			auto c1 = elements_to_vertices[k][vnum[e][2]];

			size_t sk = k * mesh_faces_per_element + e;

			myfaces[sk].k1 = k;
			myfaces[sk].f1 = e;
			myfaces[sk].k2 = k;
			myfaces[sk].f2 = e;

			if (a1 > b1)
				std::swap(a1, b1);
			if (a1 > c1)
				std::swap(a1, c1);
			if (b1 > c1)
				std::swap(b1, c1);

			myfaces[sk].va = a1;
			myfaces[sk].vb = b1;
			myfaces[sk].vc = c1;
		}
	}

	std::sort(myfaces.begin(), myfaces.end());	

	for (size_t n = 1; n < myfaces.size(); ++n)
	{
		if (myfaces[n] <= myfaces[n - 1])
		{
			// "marry" the faces
		    myfaces[n].k2 =  myfaces[n - 1].k1;  myfaces[n].f2 =  myfaces[n - 1].f1;
     		myfaces[n - 1].k2 = myfaces[n].k1;   myfaces[n - 1].f2 = myfaces[n].f1;
		}
	}

	std::vector<std::vector<size_t>> element_to_element;
  	std::vector<std::vector<size_t>> element_to_face;
	element_to_element.resize(mesh_elements, std::vector<size_t>(mesh_faces_per_element));
	element_to_face.resize(mesh_elements, std::vector<size_t>(mesh_faces_per_element));

	for (size_t n = 0; n < mesh_elements * mesh_faces_per_element; ++n)
	{
		auto const k1 = myfaces[n].k1;
		auto const f1 = myfaces[n].f1;
		auto const k2 = myfaces[n].k2;
		auto const f2 = myfaces[n].f2;

		element_to_element[k1][f1] = k2;
		element_to_face[k1][f1] = f2;
	}

	myfaces.clear();

	std::vector<coord_t> reference_coords;
	reference_coords.resize(nodal_points_per_element);

	for (size_t n = 0; n < nodal_points_per_element; ++n)
	{
		reference_coords[n] = {p_r[n], p_s[n], p_t[n]};
	}

	cl::sycl::buffer<real3_t, 2> b_derivative(cl::sycl::range<2>{nodal_points_per_element, nodal_points_per_element});
	for (size_t n = 0; n < nodal_points_per_element; ++n)
	{
		for (size_t m = 0; m < nodal_points_per_element; ++m)
		{
			auto a_derivative = b_derivative.get_access<cl::sycl::access::mode::write>();
			a_derivative[{n, m}] = {p_Dr[n][m], p_Ds[n][m], p_Dt[n][m]};
		}
	}

	cl::sycl::buffer<double, 3> b_LIFT(cl::sycl::range<3>{nodal_points_per_element, mesh_faces_per_element, nodal_points_per_face});
	for (size_t n = 0; n < nodal_points_per_element; ++n)
	{
		for (size_t f = 0; f < mesh_faces_per_element; ++f)
		{
			for (size_t m = 0; m < nodal_points_per_face; ++m)
			{
				auto a_LIFT = b_LIFT.get_access<cl::sycl::access::mode::write>();
				a_LIFT[{n, f, m}] = p_LIFT[n][f * nodal_points_per_face + m];
			}
		}
	}

	cl::sycl::buffer<size_t, 2> b_Fmask(cl::sycl::range<2>{mesh_faces_per_element, nodal_points_per_face});
	for (size_t n = 0; n < mesh_faces_per_element; ++n)
	{
		for (size_t m = 0; m < nodal_points_per_face; ++m)
		{
			auto a_Fmask = b_Fmask.get_access<cl::sycl::access::mode::write>();
			a_Fmask[{n, m}] = p_Fmask[n][m];
		}
	}

	//// there are four types of coefficients copied from the header files: reference node coordinates, local nodal derivative matrices, LIFT matrix and mask

	/* low storage RK coefficients */
	std::vector<double> rk4a, rk4b, rk4c;
	rk4a.resize(5);
	rk4a[0] = 0.0;
	rk4a[1] = -567301805773.0 / 1357537059087.0;
	rk4a[2] = -2404267990393.0 / 2016746695238.0;
	rk4a[3] = -3550918686646.0 / 2091501179385.0;
	rk4a[4] = -1275806237668.0 / 842570457699.0;

	rk4b.resize(5);
	rk4b[0] = 1432997174477.0 / 9575080441755.0;
	rk4b[1] = 5161836677717.0 / 13612068292357.0;
	rk4b[2] = 1720146321549.0 / 2090206949498.0;
	rk4b[3] = 3134564353537.0 / 4481467310338.0;
	rk4b[4] = 2277821191437.0 / 14882151754819.0;

	rk4c.resize(6);
	rk4c[0] = 0.0;
	rk4c[1] = 1432997174477.0 / 9575080441755.0;
	rk4c[2] = 2526269341429.0 / 6820363962896.0;
	rk4c[3] = 2006345519317.0 / 3224310063776.0;
	rk4c[4] = 2802321613138.0 / 2924317926251.0;
	rk4c[5] = 1.0;

	/* build coordinates */
	cl::sycl::buffer<coord_t, 2> b_node_coords(cl::sycl::range<2>{mesh_elements, nodal_points_per_element});

	for (size_t k = 0; k < mesh_elements; ++k)
	{ //// transforming to the reference tetrahedron coordinates
		for (size_t n = 0; n < nodal_points_per_element; ++n)
		{
			auto r = reference_coords[n].x;
			auto s = reference_coords[n].y;
			auto t = reference_coords[n].z;
			auto a_node_coords = b_node_coords.get_access<cl::sycl::access::mode::write>();
			auto a_vertex_coords = b_vertex_coords.get_access<cl::sycl::access::mode::read>();
			a_node_coords[{k, n}] = 0.5 * (-a_vertex_coords[{k, 0}] * (r + s + t + 1) + a_vertex_coords[{k, 1}] * (1.0 + r) + a_vertex_coords[{k, 2}] * (1.0 + s) + a_vertex_coords[{k, 3}] * (1.0 + t));
		}
	}

	/* build node-node connectivity maps */
	cl::sycl::buffer<std::array<size_t, 3>, 3> b_vmapM(cl::sycl::range<3>{mesh_elements, mesh_faces_per_element, nodal_points_per_face});
	cl::sycl::buffer<std::array<size_t, 3>, 3> b_vmapP(cl::sycl::range<3>{mesh_elements, mesh_faces_per_element, nodal_points_per_face});
	{
		auto a_vmapM = b_vmapM.get_access<cl::sycl::access::mode::write>();
		auto a_vmapP = b_vmapP.get_access<cl::sycl::access::mode::write>();
		auto a_Fmask = b_Fmask.get_access<cl::sycl::access::mode::read>();
		auto a_node_coords = b_node_coords.get_access<cl::sycl::access::mode::read>();

		for (size_t k1 = 0; k1 < mesh_elements; ++k1)
		{
			/* get some information about the face geometries */
			for (size_t f1 = 0; f1 < mesh_faces_per_element; ++f1)
			{
				/* volume -> face nodes */
				for (size_t n1 = 0; n1 < nodal_points_per_face; ++n1)
				{
					a_vmapM[{k1, f1, n1}] = {{k1, f1, a_Fmask[{f1, n1}]}};
				}
				/* find neighbor */
				auto k2 = element_to_element[k1][f1];
				auto f2 = element_to_face[k1][f1];
				if (k1 == k2)
				{
					for (size_t n1 = 0; n1 < nodal_points_per_face; ++n1)
					{
						a_vmapP[{k1, f1, n1}] = {{k1, f1, a_Fmask[{f1, n1}]}};
					}
				}
				else
				{
					/* treat as boundary for the moment  */

					for (size_t n1 = 0; n1 < nodal_points_per_face; ++n1)
					{
						auto c1 = a_node_coords[{k1, a_Fmask[{f1, n1}]}];
						for (size_t n2 = 0; n2 < nodal_points_per_face; ++n2)
						{
							auto c2 = a_node_coords[{k2, a_Fmask[{f2, n2}]}];
							/* find normalized distance between these nodes */
							/* [ use sJk as a measure of edge length (ignore factor of 2) ] */
							auto d = c1 - c2;

							if (d.length() < 1e-5)
							{
								a_vmapP[{k1, f1, n1}] = {{k2, f2, a_Fmask[{f2, n2}]}};
								break;
							}
						}
					}
				}
			}
		}
	}

	/* field storage */
	cl::sycl::buffer<real3_t, 2> b_H(cl::sycl::range<2>{mesh_elements, nodal_points_per_element});
	cl::sycl::buffer<real3_t, 2> b_E(cl::sycl::range<2>{mesh_elements, nodal_points_per_element});

	/* initial conditions */
	for (size_t k = 0; k < mesh_elements; ++k)
	{
		for (size_t n = 0; n < nodal_points_per_element; ++n)
		{
			auto a_E = b_E.get_access<cl::sycl::access::mode::write>();
			auto a_node_coords = b_node_coords.get_access<cl::sycl::access::mode::read>();
			a_E[{k, n}].y = sin(M_PI * a_node_coords[{k, n}].x) * sin(M_PI * a_node_coords[{k, n}].z);
		}
	}

	cl::sycl::buffer<real3_t, 2> b_res_H(cl::sycl::range<2>{mesh_elements, nodal_points_per_element});
	cl::sycl::buffer<real3_t, 2> b_res_E(cl::sycl::range<2>{mesh_elements, nodal_points_per_element});
	cl::sycl::buffer<real3_t, 2> b_rhs_H(cl::sycl::range<2>{mesh_elements, nodal_points_per_element});
	cl::sycl::buffer<real3_t, 2> b_rhs_E(cl::sycl::range<2>{mesh_elements, nodal_points_per_element});

	/* vgeo */
	cl::sycl::buffer<real3_t, 2> b_vgeo(cl::sycl::range<2>{mesh_elements, 3});
	cl::sycl::buffer<real_t, 2> b_surface_jacobians(cl::sycl::range<2>{mesh_elements, mesh_faces_per_element});
	cl::sycl::buffer<real_t, 2> b_surface_directions(cl::sycl::range<2>{mesh_elements, mesh_faces_per_element});
	cl::sycl::buffer<real3_t, 2> b_surface_normals(cl::sycl::range<2>{mesh_elements, mesh_faces_per_element});

	double timestep_size = 1e6;

	for (size_t k = 0; k < mesh_elements; ++k)
	{
		std::vector<real3_t> normal_coords(mesh_faces_per_element);
		real3_t dr, ds, dt;
		real_t J;

		// geometricfactors3d
		{
			auto const a_vertex_coords = b_vertex_coords.get_access<cl::sycl::access::mode::read>();
			auto const g0 = a_vertex_coords[{k, 0}];
			auto const g1 = a_vertex_coords[{k, 1}];
			auto const g2 = a_vertex_coords[{k, 2}];
			auto const g3 = a_vertex_coords[{k, 3}];

			real3_t const dx = {(g1.x - g0.x) / 2, (g2.x - g0.x) / 2, (g3.x - g0.x) / 2};
			real3_t const dy = {(g1.y - g0.y) / 2, (g2.y - g0.y) / 2, (g3.y - g0.y) / 2};
			real3_t const dz = {(g1.z - g0.z) / 2, (g2.z - g0.z) / 2, (g3.z - g0.z) / 2};

			J = dx.x * (dy.y * dz.z - dz.y * dy.z) - dy.x * (dx.y * dz.z - dz.y * dx.z) + dz.x * (dx.y * dy.z - dy.y * dx.z);
			dr = {(dy.y * dz.z - dz.y * dy.z) / J, -(dx.y * dz.z - dz.y * dx.z) / J, (dx.y * dy.z - dy.y * dx.z) / J};
			ds = {-(dy.x * dz.z - dz.x * dy.z) / J, (dx.x * dz.z - dz.x * dx.z) / J, -(dx.x * dy.z - dy.x * dx.z) / J};
			dt = {(dy.x * dz.y - dz.x * dy.y) / J, -(dx.x * dz.y - dz.x * dx.y) / J, (dx.x * dy.y - dy.x * dx.y) / J};

			if (J < 1e-10)
				std::cerr << "warning: J = " << J << std::endl;
		}

		auto a_vgeo = b_vgeo.get_access<cl::sycl::access::mode::write>();
		a_vgeo[{k, 0}] = dr;
		a_vgeo[{k, 1}] = ds;
		a_vgeo[{k, 2}] = dt;

		/* local-local info */
		std::vector<real_t> sJk(mesh_faces_per_element);

		normal_coords[0] = -dt;
		normal_coords[1] = -ds;
		normal_coords[2] = dr + ds + dt;
		normal_coords[3] = -dr;

		for (size_t f = 0; f < mesh_faces_per_element; ++f)
		{
			sJk[f] = normal_coords[f].length();
			normal_coords[f] /= sJk[f];
			sJk[f] *= J;
			timestep_size = std::min(timestep_size, J / sJk[f]);
		}

		auto a_surface_jacobians = b_surface_jacobians.get_access<cl::sycl::access::mode::write>();
		auto a_surface_directions = b_surface_directions.get_access<cl::sycl::access::mode::write>();
		auto a_surface_normals = b_surface_normals.get_access<cl::sycl::access::mode::write>();
		auto a_vmapM = b_vmapM.get_access<cl::sycl::access::mode::read>();
		auto a_vmapP = b_vmapP.get_access<cl::sycl::access::mode::read>();
		for (size_t f = 0; f < mesh_faces_per_element; ++f)
		{
			a_surface_jacobians[{k, f}] = sJk[f] / (2. * J);
			a_surface_directions[{k, f}] =  (a_vmapM[{k, f, 0}] == a_vmapP[{k, f, 0}]) ? -1. : 1.;
			a_surface_normals[{k, f}] = normal_coords[f];
		}
	}

	////////////////////////// end of InitOcca3D
	timestep_size = .5 * timestep_size / ((p_N + 1) * (p_N + 1));

	double constexpr final_time = .75;
	std::cout << "final_time=" << final_time << std::endl;

	//////////////////////////////////////From MaxwellRun3d //////////
	double current_time = 0;

	size_t const timesteps = final_time / timestep_size; // rounding timesteps
	timestep_size = final_time / timesteps;

	// BuildKernels was here

	std::cout << "The bool values are set" << std::endl;

	#if defined USE_CPU
	auto selector = cl::sycl::cpu_selector{};
	#elif defined USE_GPU
	auto selector = cl::sycl::gpu_selector{};
	#else
	auto selector = cl::sycl::host_selector{};
	#endif
#ifndef TRISYCL_CL_LANGUAGE_VERSION
	try
	{
		auto device = selector.select_device();
		std::cout << "Device " << device.get_info<cl::sycl::info::device::name>() << std::endl;
		std::cout << "Device is_host:" << device.is_host() << "  " << "is_cpu:" << device.is_cpu() << " is_gpu:" << device.is_gpu() << " is_accelerator:" << device.is_accelerator() << std::endl;
	} catch (cl::sycl::exception const& e) {
		std::cout << "Caught synchronous SYCL exception:\n"  << e.what() << std::endl;
	}
#endif


#ifdef RECORD_TIMES
	auto t_start = std::chrono::steady_clock::now();
#endif
	{ // begin of queue scope
#ifdef TRISYCL_CL_LANGUAGE_VERSION
		cl::sycl::queue queue;
#else
		cl::sycl::queue queue(selector);
#endif

		/* outer current_time step loop  */
		for (size_t timestep = 0; timestep < timesteps; ++timestep)
		{
			for (size_t runge_kutta_stage = 1; runge_kutta_stage <= 5; ++runge_kutta_stage)
			{
				/* compute rhs of TM-mode MaxwellsGPU's equations */
				real_t const fdt = timestep_size;
				real_t const fa = (real_t)rk4a[runge_kutta_stage - 1];
				real_t const fb = (real_t)rk4b[runge_kutta_stage - 1];

				try {
				queue.submit([&](cl::sycl::handler &cgh) {
					auto a_rhs_H = b_rhs_H.get_access<cl::sycl::access::mode::write>(cgh);
					auto a_rhs_E = b_rhs_E.get_access<cl::sycl::access::mode::write>(cgh);
					auto a_H = b_H.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_E = b_E.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_surface_jacobians = b_surface_jacobians.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_surface_directions = b_surface_directions.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_surface_normals = b_surface_normals.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_vmapM = b_vmapM.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_vmapP = b_vmapP.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_derivative = b_derivative.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_vgeo = b_vgeo.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_LIFT = b_LIFT.get_access<cl::sycl::access::mode::read>(cgh);

					cgh.parallel_for<class mw_surfacevolume>(cl::sycl::nd_range<1>{MULT(mesh_elements), GROUPSIZE}, [=](cl::sycl::nd_item<1> itm) {
						size_t const k = itm.get_global_id(0);
						if(k < mesh_elements) {

							for(size_t n = 0; n < nodal_points_per_element; ++n)
							{
								a_rhs_H[{k, n}] = 0;
								a_rhs_E[{k, n}] = 0;
							}

							/* maxwellsurfacekernel */
							for (size_t f = 0; f < mesh_faces_per_element; ++f)
							{
								/* grab surface nodes and store flux in shared memory */
								real_t const jacobian = a_surface_jacobians[{k, f}];
								real_t const direction = a_surface_directions[{k, f}];
								real3_t const normal = a_surface_normals[{k, f}];

								for (size_t m = 0; m < nodal_points_per_face; ++m)
								{
									auto myM = a_vmapM[{k, f, m}];
									auto myP = a_vmapP[{k, f, m}];

									auto H_MQ = a_H[std::get<0>(myM)][std::get<2>(myM)];
									auto H_PQ = a_H[std::get<0>(myP)][std::get<2>(myP)];
									auto E_MQ = a_E[std::get<0>(myM)][std::get<2>(myM)];
									auto E_PQ = a_E[std::get<0>(myP)][std::get<2>(myP)];

									real3_t const dH = jacobian * (H_PQ - H_MQ);
									real3_t const dE = jacobian * (direction * E_PQ - E_MQ);

									real_t const ndotdH = dot_product(normal, dH);
									real_t const ndotdE = dot_product(normal, dE);

									real3_t const fluxH = dH - ndotdH * normal - cross_product(normal, dE);
									real3_t const fluxE = dE - ndotdE * normal + cross_product(normal, dH);

									for(size_t n = 0; n < nodal_points_per_element; ++n)
									{
										a_rhs_H[{k, n}] += a_LIFT[{n, f, m}] * fluxH;
										a_rhs_E[{k, n}] += a_LIFT[{n, f, m}] * fluxE;
									}
								}
							}

							for (size_t n = 0; n < nodal_points_per_element; ++n)
							{
								/* maxwellvolumekernel */
								real3_t dHx, dHy, dHz;
								real3_t dEx, dEy, dEz;
								vec3_t<real3_t> dH, dE;

								for (size_t m = 0; m < nodal_points_per_element; ++m)
								{
									dHx += a_derivative[{n, m}] * a_H[{k, m}].x;
									dHy += a_derivative[{n, m}] * a_H[{k, m}].y;
									dHz += a_derivative[{n, m}] * a_H[{k, m}].z;
									dEx += a_derivative[{n, m}] * a_E[{k, m}].x;
									dEy += a_derivative[{n, m}] * a_E[{k, m}].y;
									dEz += a_derivative[{n, m}] * a_E[{k, m}].z;
								}

								real3_t const d_r = a_vgeo[{k, 0}];
								real3_t const d_s = a_vgeo[{k, 1}];
								real3_t const d_t = a_vgeo[{k, 2}];

								// FIXME nicer curl() implementation
								a_rhs_H[{k, n}] += {
									-(d_r.y * dEz.x + d_s.y * dEz.y + d_t.y * dEz.z - d_r.z * dEy.x - d_s.z * dEy.y - d_t.z * dEy.z),
									-(d_r.z * dEx.x + d_s.z * dEx.y + d_t.z * dEx.z - d_r.x * dEz.x - d_s.x * dEz.y - d_t.x * dEz.z),
									-(d_r.x * dEy.x + d_s.x * dEy.y + d_t.x * dEy.z - d_r.y * dEx.x - d_s.y * dEx.y - d_t.y * dEx.z)};
								a_rhs_E[{k, n}] += {
									d_r.y * dHz.x + d_s.y * dHz.y + d_t.y * dHz.z - d_r.z * dHy.x - d_s.z * dHy.y - d_t.z * dHy.z,
									d_r.z * dHx.x + d_s.z * dHx.y + d_t.z * dHx.z - d_r.x * dHz.x - d_s.x * dHz.y - d_t.x * dHz.z,
									d_r.x * dHy.x + d_s.x * dHy.y + d_t.x * dHy.z - d_r.y * dHx.x - d_s.y * dHx.y - d_t.y * dHx.z};
							}
						}
					}); // parallel_for
				});		// queue.submit
				} catch (cl::sycl::exception const& e) {
					std::cout << "Caught synchronous SYCL exception:\n" << e.what() << std::endl;
					std::exit(-1);
				}

				try {
				queue.submit([&](cl::sycl::handler &cgh) {
					auto a_rhs_H = b_rhs_H.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_rhs_E = b_rhs_E.get_access<cl::sycl::access::mode::read>(cgh);
					auto a_H = b_H.get_access<cl::sycl::access::mode::read_write>(cgh);
					auto a_E = b_E.get_access<cl::sycl::access::mode::read_write>(cgh);
					auto a_res_H = b_res_H.get_access<cl::sycl::access::mode::read_write>(cgh);
					auto a_res_E = b_res_E.get_access<cl::sycl::access::mode::read_write>(cgh);

					cgh.parallel_for<class mw_rk>(cl::sycl::nd_range<1>{MULT(mesh_elements), GROUPSIZE}, [=](cl::sycl::nd_item<1> itm) {
						size_t const k = itm.get_global_id(0);
							if(k < mesh_elements) {
								/* maxwellrkkernel */
								for (size_t n = 0; n < nodal_points_per_element; ++n)
								{
									a_res_H[{k, n}] = fa * a_res_H[{k, n}] + fdt * a_rhs_H[{k, n}];
									a_res_E[{k, n}] = fa * a_res_E[{k, n}] + fdt * a_rhs_E[{k, n}];
									a_H[{k, n}] += fb * a_res_H[{k, n}];
									a_E[{k, n}] += fb * a_res_E[{k, n}];
								}
							}
					}); // parallel_for
				});		// queue.submit
				} catch (cl::sycl::exception const& e) {
					std::cout << "Caught synchronous SYCL exception:\n" << e.what() << std::endl;
				}
			} // runge_kutta_stage

#ifdef ERROR_EVERY_ITERATION
			{
				auto a_E = b_E.get_access<cl::sycl::access::mode::read>();
				auto a_node_coords = b_node_coords.get_access<cl::sycl::access::mode::read>();
				/* find maximum & minimum values for Ey */
				real_t minEy = a_E[{0, 0}].y, maxEy = a_E[{0, 0}].y;
				real_t maxErrorEy = 0;
				for (size_t k = 0; k < mesh_elements; ++k)
				{
					for (size_t n = 0; n < nodal_points_per_element; ++n)
					{
						real_t exactEy = sin(M_PI * a_node_coords[{k, n}].x) * sin(M_PI * a_node_coords[{k, n}].z) * cos(sqrt(2.) * M_PI * final_time);
						real_t errorEy = std::abs(exactEy - a_E[{k, n}].y);
						maxErrorEy = std::max(errorEy, maxErrorEy);
						minEy = std::min(minEy, a_E[{k, n}].y);
						maxEy = std::max(maxEy, a_E[{k, n}].y);
					}
				}
				std::cout << "t=" << current_time << " Ey in [ " << minEy << ", " << maxEy << " ] with max nodal error " << maxErrorEy << std::endl;
			}
#endif
			current_time += timestep_size; /* increment current current_time */
		}

		try {
			queue.wait_and_throw();
		} catch (cl::sycl::exception const& e) {
			std::cout << "Caught synchronous SYCL exception:\n"  << e.what() << std::endl;
		}
	} // queue out of scope

#ifdef RECORD_TIMES
	auto t_end = std::chrono::steady_clock::now();
	double t_diff = std::chrono::duration_cast<std::chrono::milliseconds>(t_end - t_start).count();
	t_diff /= 1000.0;
	std::ofstream result_file;
	result_file.open(RESULTS_FILE, std::ofstream::out | std::ofstream::app);
	result_file << p_N << ";" << mesh_elements << ";" << t_diff << std::endl;
	result_file.close();
#endif

	auto a_E = b_E.get_access<cl::sycl::access::mode::read>();
	auto a_node_coords = b_node_coords.get_access<cl::sycl::access::mode::read>();

	/* find maximum & minimum values for Ey */
	real_t minEy = a_E[{0, 0}].y, maxEy = a_E[{0, 0}].y;
	real_t maxErrorEy = 0;
	for (size_t k = 0; k < mesh_elements; ++k)
	{
		for (size_t n = 0; n < nodal_points_per_element; ++n)
		{
			real_t exactEy = sin(M_PI * a_node_coords[{k, n}].x) * sin(M_PI * a_node_coords[{k, n}].z) * cos(sqrt(2.) * M_PI * final_time);
			real_t errorEy = std::abs(exactEy - a_E[{k, n}].y);
			maxErrorEy = std::max(errorEy, maxErrorEy);
			minEy = std::min(minEy, a_E[{k, n}].y);
			maxEy = std::max(maxEy, a_E[{k, n}].y);
		}
	}
	std::cout << "t=" << final_time << " Ey in [ " << minEy << ", " << maxEy << " ] with max nodal error " << maxErrorEy << std::endl;

	return EXIT_SUCCESS;
}
