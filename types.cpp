#include "types.h"

bool operator<(face3d_t const& a, face3d_t const& b)
{
  return std::tie(a.va, a.vb, a.vc) < std::tie(b.va, b.vb, b.vc);
}

bool operator<=(face3d_t const& a, face3d_t const& b)
{
  return std::tie(a.va, a.vb, a.vc) <= std::tie(b.va, b.vb, b.vc);
}

bool operator>(face3d_t const& a, face3d_t const& b)
{
  return std::tie(a.va, a.vb, a.vc) > std::tie(b.va, b.vb, b.vc);
}

bool operator>=(face3d_t const& a, face3d_t const& b)
{
    return std::tie(a.va, a.vb, a.vc) >= std::tie(b.va, b.vb, b.vc);
}
